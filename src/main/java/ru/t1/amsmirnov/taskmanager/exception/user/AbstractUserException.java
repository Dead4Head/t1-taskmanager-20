package ru.t1.amsmirnov.taskmanager.exception.user;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public class AbstractUserException extends AbstractException {

    public AbstractUserException() {

    }

    public AbstractUserException(final String message) {
        super(message);
    }

    public AbstractUserException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(final Throwable cause) {
        super(cause);
    }

    public AbstractUserException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
