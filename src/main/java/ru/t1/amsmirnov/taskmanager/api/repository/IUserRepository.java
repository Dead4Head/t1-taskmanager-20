package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

public interface IUserRepository extends IRepository<User> {

    User findOneByLogin(String login) throws AbstractException;

    User findOneByEmail(String email) throws AbstractException;

    Boolean isLoginExist(String login) throws AbstractException;

    Boolean isEmailExist(String email) throws AbstractException;

}
