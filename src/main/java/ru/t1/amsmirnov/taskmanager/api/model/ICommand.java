package ru.t1.amsmirnov.taskmanager.api.model;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute() throws AbstractException;

}
