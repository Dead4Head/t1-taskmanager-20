package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.enumerated.Sort;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model) throws AbstractException;

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id) throws AbstractException;

    M findOneByIndex(Integer index) throws AbstractException;

    void clear();

    M remove(M model) throws AbstractException;

    M removeById(String id) throws AbstractException;

    M removeByIndex(Integer index) throws AbstractException;

    Integer getSize();

    Boolean existById(String id) throws AbstractException;

}
