package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.enumerated.Sort;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model) throws AbstractException;

    List<M> findAll(String userId) throws AbstractException;

    List<M> findAll(String userId, Comparator<M> comparator) throws AbstractException;

    List<M> findAll(String userId, Sort sort) throws AbstractException;

    M findOneById(String userId, String id) throws AbstractException;

    M findOneByIndex(String userId, Integer index) throws AbstractException;

    M remove(String userId, M model) throws AbstractException;

    M removeById(String userId, String id) throws AbstractException;

    M removeByIndex(String userId, Integer index) throws AbstractException;

    boolean existById(String userId, String id) throws AbstractException;

    void clear(String userId) throws AbstractException;

    int getSize(String userId) throws AbstractException;

}
