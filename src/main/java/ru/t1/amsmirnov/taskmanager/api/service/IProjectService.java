package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name, String description) throws AbstractException;

    Project updateById(String userId, String id, String name, String description) throws AbstractException;

    Project updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Project changeStatusById(String userId, String id, Status status) throws AbstractException;

    Project changeStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

}
